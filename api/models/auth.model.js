import { db } from '../helpers/db.helper.js';

const addLoggedIn = (candidate, callback) => {
    db(
      `INSERT INTO login set ?`, candidate,
      (err, response) => {
        if(err != null) {
           callback(err);
        } else {
          callback(null, response);
        }
      }
    );
  };
  export const authModel = { addLoggedIn };
