import { db } from '../helpers/db.helper.js';


const addCandidate = (candidate, callback) => {

  db(
    `INSERT INTO candidate_details set ?`, candidate,
    (err, response) => {
      if(err != null) {
         callback(err);
      } else {
        callback(null, response);
      }
    }
  );
};

const isCandidateExistByEmailId = (email, callback) => {

  db(`Select * from candidate_details where email_id = '${email}'`, (err, response) => {
    if (!err && response) {
      callback(null, response);
    } else {
      callback(err);
    }
  });
};

const getCandidateById = (candidateid, callback) => {
  db(`Select * from candidate_details where id = '${candidateid}'`, (err, response) => {
    if (!err && response && response.length > 0) {
      callback(null, response);
    }else {
      callback(err);
    }
  });
};

export const candidatesModel = { addCandidate, isCandidateExistByEmailId, getCandidateById };
