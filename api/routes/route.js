import express from 'express';
const router = express.Router();
import { authRoute } from './route/auth.route.js';
import { candidateRoute } from './route/candidates.route.js';

const auth = authRoute;
const candidate = candidateRoute;

router.use('/auth', auth);
router.use('/candidates',candidate);

export const mainRouter = router;
