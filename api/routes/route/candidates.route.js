import express from 'express';
const router = express.Router();
import { validator } from '../../helpers/validator.helper.js';
import candidatesController from '../../controllers/candidates.controller.js';
import candidateValidation from '../../validations/candidate.validation.js';

router.post('/', validator.body(candidateValidation.register), candidatesController.register);

export const candidateRoute = router;
