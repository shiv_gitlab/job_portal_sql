import jwt from 'jsonwebtoken';

export const generateToken = data => {
  return jwt.sign(data, process.env.SECRET_KEY, {
    expiresIn: process.env.TOKEN_EXPIRY}
  );

};

