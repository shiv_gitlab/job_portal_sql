import { GeneralResponse } from '../utils/response.js';
import { GeneralError, NotFound } from '../utils/error.js';
import { candidatesModel } from '../models/candidates.model.js';
import { config } from '../utils/config.js';
import bcrypt from 'bcrypt';
const saltRounds = 10;

const getCandidateByCId = async (req, res, next) => {
    const { candidateid } = req.query;
    try {
        candidatesModel.getCandidateById(candidateid, (err, response) => {
            if (err) {
              next(new NotFound('candiadte not found'));
            } else {
              next(new GeneralResponse('candiadte detail found',response));
            }
        });
    } catch (err) {
        next(new GeneralError('error while getting candiadte detail'));
    }
};

const register = async (req, res, next) => {
  try {
    const {name, surname, email_id, mobile, gender, password } = req.body;
    const encryptedPassword = await bcrypt.hash(password, saltRounds);
    candidatesModel.isCandidateExistByEmailId(email_id, async (err, response) => {
      if (!err && response.length > 0){
        next(
          new GeneralError(
            'candidate already exist',
            undefined,
            config.HTTP_ACCEPTED
          )
        );
      } else {
        candidatesModel.addCandidate(
          { name, surname, email_id, mobile, gender, password: encryptedPassword },
          (error, addResponse) => {
            if (error || addResponse.affectedRows === 0){
              next(new GeneralError('candidate registeration failed'));
            } else {
              next(
                new GeneralResponse(
                  'candidate successfully registered',
                  undefined,
                  config.HTTP_CREATED
                )
              );
            }
          }
        );
      }
    });
  } catch (err) {
    next(new GeneralError('candidate registeration failed'));
  }
};

export default{ getCandidateByCId, register };
