import bcrypt from 'bcrypt';
import { generateToken } from '../helpers/auth.helper.js';
import { GeneralResponse } from '../utils/response.js';
import { GeneralError, UnAuthorized } from '../utils/error.js';
import {candidatesModel} from '../models/candidates.model.js';
import {authModel} from '../models/auth.model.js';
import {config} from '../utils/config.js';
const saltrounds = 10;

const login = async (req, res, next) => {
  try {
    const { username, password } = req.body;
    candidatesModel.isCandidateExistByEmailId(username, async (err, response) => {
      if (err || response.length === 0){
        next(new GeneralError('invalid username or password', undefined, config.HTTP_ACCEPTED));
      } else {
        const comparision = await bcrypt.compare(password, response[0].password);
        if (comparision){
          const candidatedata = {
            username: response[0].email_id,
            userid: response[0].id};
          const token = generateToken(candidatedata);
          const encryptedPassword = await bcrypt.hash(password, saltrounds);
          authModel.addLoggedIn(
            { username, password: encryptedPassword },
            (error, addResponse) => {
              if (error || addResponse.affectedRows === 0){
                next(new GeneralError('user login failed'));
              } else {
                next(
                  new GeneralResponse('user successfully logged in', { token }, config.HTTP_SUCCESS)
                );
              }
            }
          );
        } else {
          next(new UnAuthorized('Invalid username or password'));
        }
      }
    });
  } catch (err) {
    next(new GeneralError('user login failure'));
  }
};

export default { login };
