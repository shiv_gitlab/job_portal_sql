import Joi from 'joi';

const minLen = 6;
const maxLen = 16;
const mobile = 10;

export default {
  register: Joi.object({
    name: Joi.string().required().empty().messages({
      'string.base': `first name should be a type of 'text'`,
      'string.empty': `first name cannot be an empty field`,
      'any.required': 'first name is a required field'}
    ),
    surname: Joi.string().required().empty().messages({
      'string.base': `last name should be a type of 'text'`,
      'string.empty': `last name cannot be an empty field`,
      'any.required': 'last name is a required field'}
    ),
    email_id: Joi.string().required().empty().email().messages({
      'string.base': `email should be a type of 'text'`,
      'string.empty': `email cannot be an empty field`,
      'string.email': `email format not valid`,
      'any.required': 'email is a required field'}
    ),
    password: Joi.string().required().empty().regex(/^[a-zA-Z0-9]{6,16}$/).min(minLen).max(maxLen).messages({
      'string.base': `pass should be a type of 'text'`,
      'string.empty': `pass cannot be an empty field`,
      'string.min': `pass should be of minimum 6 characters`,
      'string.max': 'pass should be of maximum 16 characters',
      'string.pattern.base': 'pass must contains lower case, upper case and between 6 and 16 characters',
      'any.required': `pass is a required field`}
    ),
    mobile: Joi.string().required().empty().regex(/^[0-9]{10}$/).min(mobile).max(mobile).messages({
      'string.pattern.base': 'mobile number must contain digits and must have 10 characters without country code'}
    ),
    gender: Joi.string().required().empty().messages({
      'string.empty': `gender cannot be an empty field`,
      'any.required': 'gender is a required field'}
      )}
  )};


